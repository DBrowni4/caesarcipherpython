# CaesarCipherPython

Example implementation of a Caesar Cipher using Python.

## Features

* API Interface running on port 8080 using Flask
* Handles uppercase, lowercase, and symbols
* Proper input validation and error checking
* Automated pipeline testing via PyTest.

## To-do

* Update the API so that it can handle more than one request per POST request
* Implement some form of efficient caching.
  * Perhaps this could mean caching small words, because statistically smaller words have fewer combinations and are more likely to be unique.
  * These smaller words could also could be searched for in larger queries.
* Add config files for API setup.
