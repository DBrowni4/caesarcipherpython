import string


DEFAULT_RUBRIC = string.ascii_lowercase + string.ascii_uppercase

# The Cipher Class manages the base functionality of our Caesar Cipher

class Cipher:
    def __init__(self, rubric=DEFAULT_RUBRIC):
        self.__rubric = rubric # rubric refers to our letter key

    def rightShift(self, cleartext, rot=13):
        ciphertext = ""

        # in every character, we check if it's in our rubric
        for eachChar in cleartext:
            currentIndex = self.__rubric.find(eachChar)

            # if the character is in our rubric, we convert it
            if currentIndex != -1:
                targetIndex = (currentIndex + rot) % len(self.__rubric)

            # otherwise we pass it along
            else:
                targetIndex = currentIndex

            ciphertext += self.__rubric[targetIndex]

        return ciphertext

    # little bit of mathemagic to aid DRY principles. if we negate the rotation, we get the opposite effect.
    def leftShift(self, cleartext, rot=13):
        return self.rightShift(cleartext, rot * -1)

