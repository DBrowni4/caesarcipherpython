import pytest

from cipher import *


@pytest.fixture
def testSubject():
    return Cipher()


def test_rightShift(testSubject):
    assert(testSubject.rightShift("Test", 3) == "Whvw")


def test_leftShift(testSubject):
    assert(testSubject.leftShift("Whvw", 3) == "Test")

