from flask import Flask
from flask_restful import Resource, Api, reqparse
from cipher import Cipher

# our main.py file contains elements that initialize and maintain our api

# we use this class to package our cipher for the api
class CipherWrapper(Resource):
    def __init__(self):
        self.__wrappedCipher = Cipher()

    # get requests are specifically denied to prevent web snooping
    def get(self):
        return {"message": "Resource not found."}, 404


    def post(self):
        parser = reqparse.RequestParser()

        parser.add_argument("input", required=True)
        parser.add_argument("type", required=True)
        parser.add_argument("l-shift", required=True)

        args = parser.parse_args()

        out = {}
        out["input"] = args["input"]
        out["type"] = args["type"]
        out["l-shift"] = args["l-shift"]

        if args["type"] == "cipher":
            try:
                out["output"] = self.__wrappedCipher.rightShift(args["input"], int(args["l-shift"]))
            except:
                return {"message": "Bad Request"}, 400

        elif args["type"] == "decipher":
            try:
                out["output"] = self.__wrappedCipher.leftShift(args["input"], int(args["l-shift"]))
            except:
                return {"message": "Bad Request"}, 400

        else:
            return {"message": "Bad Request"}, 400

        return out, 200


# flask init and setup

app = Flask(__name__)
api = Api(app)

# set endpoints

api.add_resource(CipherWrapper, "/caesar")

# now we run our app

if __name__ == '__main__':
    host = input("Host [blank for None]: ")
    app.run(host=host, port=8080)
